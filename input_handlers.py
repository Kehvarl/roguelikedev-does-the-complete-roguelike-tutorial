import libtcodpy as libtcod


def handle_keys(key):
    key_char = chr(key.c).lower()
    # Movement keys
    if key.vk == libtcod.KEY_UP or key_char == 'k':
        return {'move': (0, -1)}
    if key.vk == libtcod.KEY_DOWN or key_char == 'j':
        return {'move': (0, 1)}
    if key.vk == libtcod.KEY_LEFT or key_char == 'h':
        return {'move': (-1, 0)}
    if key.vk == libtcod.KEY_RIGHT or key_char == 'l':
        return {'move': (1, 0)}
    if key_char == 'y':
        return {'move': (-1, -1)}
    if key_char == 'u':
        return {'move': (1, -1)}
    if key_char == 'b':
        return {'move': (-1, 1)}
    if key_char == 'n':
        return {'move': (1, 1)}

    if key.vk == libtcod.KEY_ENTER and key.lalt:
        # Alt+Enter: Toggle full-screen
        return {'fullscreen': True}

    if key.vk == libtcod.KEY_ESCAPE:
        # Exit the game
        return {'exit': True}

    return{}
